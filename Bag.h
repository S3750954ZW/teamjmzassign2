#ifndef HEADER_BAG
#define HEADER_BAG

#include "LinkedList.h"
#include "util.h"

// Bag class
class Bag
{
private:

    LinkedList* tiles;

public:
    Bag();
    ~Bag();
    Bag(Bag& other);
    /**
     * actual size of tiles, no empty slot
     */
    int size();
    /**
     * fill bag with a vector of tile ptrs
     * will return residue if tile is not a valid one by colour
     */
    std::vector<Tile*> fill(std::vector<Tile*> payload);
    /**
     * fetch a vector of tile ptrs from bag
     * will only fetch as many as the bag has at the moment
     * even if count is larger than reasonable
     */
    std::vector<Tile*> fetch(int count = 1);
    /**
     * clear will resume bag to empty state
     * will not have empty slot
     */
    void clear();


    //Used to test and verify code
    std::string toString();
};



#endif // HEADER_BAG