#include "Factory.h"

Factory::Factory()
{
    this -> tiles = nullptr;
    this -> isCentre = false;
    this -> firstPlayerToken = nullptr;
}

Factory::Factory(std::vector<Tile*> payload)
{
    this -> tiles = nullptr;
    this -> isCentre = false;
    this -> firstPlayerToken = nullptr;
    if (payload.size()>0)
    {
        this -> tiles = new std::vector<Tile*>;
        for (int i = 0; i < std::min((int)payload.size(),OFFER_MAX); i++)
        {
            this -> tiles -> push_back(payload.at(i));
        }
        
    }

}

Factory::~Factory()
{
    if (this -> tiles != nullptr)
    {
        this -> tiles -> clear();
        delete this -> tiles;
        this -> tiles = nullptr;
        this -> isCentre = false;
    }
    if (this -> firstPlayerToken != nullptr)
    {
        delete this -> firstPlayerToken;
        this -> firstPlayerToken = nullptr;
    }

}

void Factory::clear()
{
    if (this -> tiles != nullptr)
    {
        if (this -> tiles -> size() > 0)
        {
            this -> tiles -> clear();
        }
        
    }
}

void Factory::becomeCentre()
{
    this -> isCentre = true;
    if (this -> firstPlayerToken == nullptr)
    {
        this -> firstPlayerToken = new Tile(FIRSTPLAYER);
    }
    
}

bool Factory::checkCentre()
{
    return this -> isCentre;
}

Tile* Factory::getFirstPlayerToken()
{
    return this -> firstPlayerToken;
}

std::vector<Tile*> Factory::fill(std::vector<Tile*> payload)
{
    std::vector<Tile*> overflow;
    std::vector<Tile*> toFill;
    std::vector<Tile*> firstPlayerToken;
    if (payload.size()>0)
    {
        if (this -> tiles == nullptr)
        {
            this -> tiles = new std::vector<Tile*>;
        }
        for (int i = 0; i < (int)payload.size(); i++)
        {
            if (payload.at(i) -> getColour() == NO_COLOUR)
            {
                overflow.push_back(payload.at(i));
            }
            else if (payload.at(i) -> getColour() == FIRSTPLAYER)
            {
                firstPlayerToken.push_back(payload.at(i));
            }
            else
            {
                toFill.push_back(payload.at(i));
            }         
        }
        
        if (!this -> isCentre)
        {
            int currentSize = this -> tiles -> size();
            for (int i = currentSize; i < std::min((int)toFill.size() + currentSize,OFFER_MAX); i++)
            {
                this -> tiles -> push_back(toFill.at(i - currentSize));
            }
        }
        else
        {
            if (firstPlayerToken.size()>0)
            {
                this -> tiles -> push_back(firstPlayerToken.at(firstPlayerToken.size()-1));
                for (int i = 0; i < (int)firstPlayerToken.size()-1; i++)
                {
                    overflow.push_back(firstPlayerToken.at(i));
                }
                
            }
            
            for (int i = 0; i < (int)toFill.size(); i++)
            {
                this -> tiles -> push_back(toFill.at(i));
            }
        }
    }
    return overflow;
}

std::vector<Tile*> Factory::fetch()
{
    std::vector<Tile*> result;
    if (this -> tiles != nullptr)
    {
        if (this -> tiles -> size() > 0)
        {
            for (int i = 0; i < (int)this -> tiles -> size(); i++)
            {
                result.push_back(this -> tiles -> at(i));
            }
            this -> tiles -> clear();
        }
    }
    return result;

}

std::vector<Tile*> Factory::fetch(Colour tileColour)
{
    std::vector<Tile*> result;
    if (this -> tiles != nullptr)
    {
        if (this -> tiles -> size() > 0)
        {
            for (int i = this -> tiles -> size() -1; i > 0; i--)
            {
                if (this -> tiles -> at(i) -> getColour() == tileColour || this -> tiles -> at(i) -> getColour() == FIRSTPLAYER)
                {
                    result.push_back(this -> tiles -> at(i));
                    this -> tiles -> erase(this -> tiles -> begin() + i);
                }
                
            }
            
        }
    }
    return result;
}

bool Factory::hasColour(Colour tileColour)
{
    bool result = false;
    if (this -> tiles != nullptr)
    {
        if (this -> tiles -> size()>0)
        {
            for (int i = 0; i < (int)this -> tiles -> size(); i++)
            {
                if (this -> tiles -> at(i) ->getColour() == tileColour)
                {
                    result = true;
                }
                
            }
            
        }
    }
    return result;
    

}

bool Factory::isEmpty()
{
    bool result = true;
    if (this -> tiles != nullptr)
    {
        if (this -> tiles -> size() > 0)
        {
            result = false;
        }
        
    }
    return result;

}

std::string Factory::toString()
{
    std::string result = "Factory is empty";
    if (this -> tiles != nullptr)
    {
        if (this -> tiles -> size() > 0)
        {
            result = "The factory has the following tiles in it \n";
            for (int i = 0; i < (int)this -> tiles -> size(); i++)
            {
                result += this -> tiles -> at(i) -> getColourStr() + "\n";
            }
        }
    }
    

    return result;

}