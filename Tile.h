#ifndef HEADER_TILE
#define HEADER_TILE

#include "Config.h"
// Tile class
// Can set a unique ID and its colour would be calculated based on ID (,aybe static cast tile deck size)
// Can get attribute
// No need to have individual saving/loading

//#define ERROR_TILE_INVALID_COLOUR   'E'    //Changed this into config.h

class Tile
{
private:
    Colour tileColour;
public:
    Tile();
    Tile(Colour tileColour);
    Tile(char tileColourC);
    Tile(Tile& other);
    ~Tile();
    std::string getColourStr();
    Colour getColour();
    char getColourChar();
};

#endif //