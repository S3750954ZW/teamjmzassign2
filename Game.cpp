#include "Game.h"

Game::Game()
{
    this -> bag = nullptr;
    this -> players = nullptr;
    this -> factories = nullptr;
    this -> roundNumber = 0;
    this -> gameName = "Unnamed";
    this -> finished = true;
    this -> testMode = false;
    this -> playerInMove = 0;
}

Game::Game(std::string gameName, bool testMode/*= false*/)
{
    this -> bag = nullptr;
    this -> players = nullptr;
    this -> factories = nullptr;
    this -> roundNumber = 0;
    this -> gameName = gameName;
    this -> finished = true;
    this -> testMode = testMode;
    this -> playerInMove = 0;
}

Game::~Game()
{
    if (this -> bag != nullptr)
    {
        delete this -> bag;
    }
    if (this -> factories != nullptr)
    {
        for (int i = 0; i < FACTORY_MAX; i++)
        {
            if (this -> factories[i] != nullptr)
            {
                delete this -> factories[i];
                this -> factories[i] = nullptr;
            }
            
        }
        delete[] this -> factories; 
        this -> factories = nullptr;
    }
    if (this -> players != nullptr)
    {
        for (int i = 0; i < PLAYER_MAX; i++)
        {
            if (this -> players[i] != nullptr)
            {
                delete this -> players[i];
                this -> players[i] = nullptr;
            }
            
        }
        delete[] this -> players; 
        this -> players = nullptr;
    }
    if (!this -> sourceTiles.empty())
    {
        for (int i = 0; i < (int)this -> sourceTiles.size(); i++)
        {
            if (this -> sourceTiles.back() != nullptr)
            {
                delete this -> sourceTiles.back();
            }
            this -> sourceTiles.pop_back();
        }
    }
    this -> roundNumber = 0;
    this -> gameName = "Unnamed";
    this -> finished = true;
    this -> playerInMove = 0;
}

void Game::resetSourceTiles(std::string sourceTiles/*=""*/)
{
    if (!this -> sourceTiles.empty())
    {
        for (int i = 0; i < (int)this -> sourceTiles.size(); i++)
        {
            if (this -> sourceTiles.back() != nullptr)
            {
                delete this -> sourceTiles.back();
            }
            this -> sourceTiles.pop_back();
        }
    }
    if (sourceTiles.empty())
    {
        for (int i = 0; i < TILE_MAX; i++)
        {
            this -> sourceTiles.push_back(new Tile(DEFAULT_TILE_SEQUENCE[i]));
        }
    }
    else
    {
        for (int i = 0; i < (int)sourceTiles.size(); i++)
        {
            if (colourCharIsValid(sourceTiles.at(i)))
            {
                this -> sourceTiles.push_back(new Tile(sourceTiles.at(i)));
            }
        }
    }
}

void Game::resetBag(std::vector<Tile*> payload)
{
    if (this -> bag == nullptr)
    {
        this -> bag = new Bag();
    }
    else
    {
        this -> bag -> clear();
    }
    this -> bag -> fill(payload);
}

void Game::resetFactories()
{
    if (this -> factories != nullptr)
    {
        for (int i = 0; i < FACTORY_MAX; i++)
        {
            if (this -> factories[i] != nullptr)
            {
                delete this -> factories[i];
                this -> factories[i] = nullptr;
            }
            
        }
        delete[] this -> factories; 
        this -> factories = nullptr;
    }

    this -> factories = new Factory*[FACTORY_MAX];
    std::vector<Tile*> payload;
    for (int i = 0; i < FACTORY_MAX; i++)
    {
        this -> factories[i] = new Factory();
        if (i == CENTRE_FACTORY_ID)
        {
            this -> factories[i] -> becomeCentre();
            payload.push_back(this -> factories[i] -> getFirstPlayerToken());
            this -> factories[i] -> fill(payload);
        }
        
    }
    
}

void Game::resetPlayers(std::string* playerNames/*=nullptr*/)
{
    if (this -> players != nullptr)
    {
        for (int i = 0; i < PLAYER_MAX; i++)
        {
            if (this -> players[i] != nullptr)
            {
                delete this -> players[i];
                this -> players[i] = nullptr;
            }
            
        }
        delete[] this -> players; 
        this -> players = nullptr;
    }
    this -> players = new Player*[PLAYER_MAX];
    for (int i = 0; i < PLAYER_MAX; i++)
    {
        if (playerNames!=nullptr)
        {
            if (!playerNames[i].empty())
            {
                this -> players[i] = new Player(playerNames[i]);
            }
            else
            {
                this -> players[i] = new Player();
            }
            
        }
        else
        {
            this -> players[i] = new Player();
        }   
    }
}

void Game::startGame()
{
    this -> finished = false;
}

void Game::startRound()
{
    if (this -> bag != nullptr && this -> factories != nullptr)
    {
        std::vector<Tile*> payload;
        for (int i = 0; i < FACTORY_MAX; i++)
        {
            if (this -> factories[i] != nullptr)
            {
                if (i == CENTRE_FACTORY_ID)
                {
                    this -> factories[i] -> becomeCentre();
                    payload.push_back(this -> factories[i] -> getFirstPlayerToken());
                    this -> factories[i] -> fill(payload);
                }
                else
                {
                    payload = this -> bag -> fetch(OFFER_MAX);
                    this -> factories[i] ->fill(payload);
                }
            }
        }
        this -> playerInMove = this -> whoMovesFirst();
    }    
    if (this -> players != nullptr)
    {
        for (int i = 0; i < PLAYER_MAX; i++)
        {
            if (this -> players[i] != nullptr)
            {
                this -> players[i] -> startRound();
            }
            
        }
        
    }
    
}

PlayerID Game::whoMovesFirst()
{
    PlayerID result = 0;
    if (this -> players != nullptr)
    {
        for (int i = 0; i < PLAYER_MAX; i++)
        {
            if (this -> players[i] != nullptr)
            {
                if (this -> players[i] -> withToken())
                {
                    result = i;
                }
                
            }
            
        }
    }
    return result;
}

Factory* Game::getFactory(FactoryID factoryID)
{
    Factory* result = nullptr;   
    if (this -> factories != nullptr)
    {
        if (factoryID <= FACTORY_MAX && factoryID != CENTRE_FACTORY_ID)
        {
            result = this -> factories[factoryID];
        }
        
    }
    return result;
}

Factory* Game::getCentre()
{
    Factory* result = nullptr;
    if (this -> factories != nullptr)
    {
        result = this -> factories[CENTRE_FACTORY_ID];   
    }
    return result;
}

Bag* Game::getBag()
{
    Bag* result = nullptr;
    if (this -> bag != nullptr)
    {
        result = this -> bag;
    }
    
    return result;
}

Player* Game::getPlayer(PlayerID playerID)
{
    Player* result = nullptr;
    if (this -> players != nullptr)
    {
        result = this -> players[playerID];
    }
    
    return result;
}

std::string Game::load(std::string loadStr)
{
    std::string sourceTiles;
    std::string playerNames[PLAYER_MAX]{"",""};
    std::vector<std::string> playCmds;
    std::string result = "The loading file is either corrupted or missing essential information ...";
    std::vector<std::string> loadLines;
    // split the loadStr to single lines and remove \r and \n
    if (!loadStr.empty())
    {
        std::string nextLine;
        std::size_t pos = 0;
        char delimiter = '\n';
        while ((pos = loadStr.find(delimiter)) != std::string::npos) {
            nextLine = loadStr.substr(0, pos);
            if (nextLine.find('\r') != std::string::npos)
            {
                nextLine.erase(nextLine.find('\r'));
            }
            loadLines.push_back(nextLine);
            loadStr.erase(0, pos + 1);
        }
        loadLines.push_back(loadStr);
    }

    // if the cmd is sufficient, start the game in loading/testing mode
    if (loadLines.size() >= SAVE_ENTRY_MIN)
    {
        sourceTiles.assign(loadLines.front());
        for (int i = 0; i < PLAYER_MAX; i++)
        {
            playerNames[i].assign(loadLines.at(i+1));
        }
        for (int i = 0; i < (int)loadLines.size() - SAVE_ENTRY_MIN; i++)
        {
            playCmds.push_back(loadLines.at(i+SAVE_ENTRY_MIN));
        }

        this -> resetSourceTiles(sourceTiles);
        this -> resetBag(this -> sourceTiles);
        this -> resetPlayers(playerNames);
        this -> resetFactories();
        
        this -> startGame();
        while (!this->gameCanFinish() && !playCmds.empty())
        {
            this -> startRound();
            while (!this -> roundCanFinish() && !playCmds.empty())
            {
                this -> turns(this -> playerInMove,playCmds.front());
                playCmds.erase(playCmds.begin());
            }
            if (this -> roundCanFinish())
            {
                this -> finishRound();
            }
        }
        if (this -> gameCanFinish())
        {
            this -> finishGame();
        }
        result = this -> getTableStr(this -> testMode, this -> finished);
    }
    return result;
}

void Game::turns(PlayerID playerInMove, std::string playerCmd)
{
    int objIntegrity = 0;
    if (this -> factories != nullptr && this -> players != nullptr)
    {
        for (int i = 0; i < PLAYER_MAX; i++)
        {
            objIntegrity += (int) (this -> players[i] != nullptr);
        }
        for (int i = 0; i < FACTORY_MAX; i++)
        {
            objIntegrity += (int) (this -> factories[i] != nullptr);
        }
    }

    if (objIntegrity >= PLAYER_MAX+FACTORY_MAX)
    {
        /* check the format of the command*/
        if (turnCmdFormat(playerCmd))
        {
            bool ruleIntegrity = true;
            /* break the the command into meaningful values*/
            FactoryID fromFactory;
            MosaicStoreRow toRow;
            Colour pickColour;
            fromFactory = parseFactoryIDChar(playerCmd.at(5));
            pickColour = parseColourChar(playerCmd.at(7));
            toRow = parseRowChar(playerCmd.at(9));
            /* check game rule against the command*/
            if (this -> factories[fromFactory] -> hasColour(pickColour))
            {
                if (toRow != BROKEN)
                {
                    if (!this -> players[playerInMove] ->getPatternline(toRow) -> isAccepting(pickColour))
                    {
                        ruleIntegrity = false;
                    }
                }
            }
            if (ruleIntegrity)
            {
                std::vector<Tile*> payload;
                payload = this -> factories[fromFactory] -> fetch(pickColour);
                payload = this -> players[playerInMove] ->takeOffer(payload, toRow);
                if (!payload.empty())
                {
                    for (int i = (int)payload.size() - 1; i >= 0; i--)
                    {
                        if (!colourIsValid(payload.at(i)->getColour()))
                        {
                            payload.erase(payload.begin()+i);
                        }
                    } 
                    if (this -> bag != nullptr)
                    {
                        this -> bag -> fill(payload);
                    } 
                }
                this -> playerInMove = (++playerInMove) % (int)PLAYER_MAX;
            }            
            
        }
        
    }
    
}

bool Game::gameCanFinish()
{
    bool result = false;
    if (this -> roundNumber >= ROUND_MAX)
    {
        result = true;
    }
    return result;
}

bool Game::roundCanFinish()
{
    int result = 0;
    if (this -> factories != nullptr)
    {
        for (int i = 0; i < FACTORY_MAX; i++)
        {
            if (this -> factories[i] != nullptr)
            {
                result += (int)(!this -> factories[i] ->isEmpty());
            }
        }
    }
    return (bool) result;
}

void Game::finishRound()
{
    std::vector<Tile*> overflow;
    if (this -> players != nullptr)
    {
        for (int i = 0; i < PLAYER_MAX; i++)
        {
            if (this -> players[i] != nullptr)
            {
                overflow = this -> players[i] -> finishRound();
                if (!overflow.empty())
                {
                    for (int i = (int)overflow.size() - 1; i >= 0; i--)
                    {
                        if (!colourIsValid(overflow.at(i)->getColour()))
                        {
                            overflow.erase(overflow.begin()+i);
                        }
                    } 
                    if (this -> bag != nullptr)
                    {
                        this -> bag -> fill(overflow);
                    } 
                }   
            } 
        }
        this -> roundNumber ++;
    }
}

void Game::finishGame()
{
    this -> finished = true;
}

std::string Game::getTableStr(bool testMode/*= false*/, bool endOfGame/*= false*/)
{
    return "the table preview";
}