#include "Tile.h"

Tile::Tile()
{
    this -> tileColour = NO_COLOUR;
}

Tile::Tile(Colour tileColour)
{
    this -> tileColour = tileColour;
}

Tile::Tile(char tileColourC)
{
    if (tileColourC == RED_TILE)
    {
        this -> tileColour = RED;
    }
    else if (tileColourC == YELLOW_TILE)
    {
        this -> tileColour = YELLOW;
    }
    else if (tileColourC == DARKBLUE_TILE)
    {
        this -> tileColour = DARKBLUE;
    }
    else if (tileColourC == LIGHTBLUE_TILE)
    {
        this -> tileColour = LIGHTBLUE;
    }
    else if (tileColourC == BLACK_TILE)
    {
        this -> tileColour = BLACK;
    }
    else if (tileColourC == FIRSTPLAYER_TILE)
    {
        this -> tileColour = FIRSTPLAYER;
    }
    else
    {
        this -> tileColour = NO_COLOUR;
    }
}

Tile::Tile(Tile& other) :
    tileColour(other.tileColour)
{
}

Tile::~Tile()
{
    this -> tileColour = NO_COLOUR;
}

Colour Tile::getColour()
{
    return this -> tileColour;
}

char Tile::getColourChar()
{
    char resultChar = NO_COLOUR_CHAR;
    if (this -> tileColour == RED)
    {
        resultChar = RED_TILE;
    }
    else if (this -> tileColour == DARKBLUE)
    {
        resultChar = DARKBLUE_TILE;
    }
    else if (this -> tileColour == LIGHTBLUE)
    {
        resultChar = LIGHTBLUE_TILE;
    }
    else if (this -> tileColour == YELLOW)
    {
        resultChar = YELLOW_TILE;
    }
    else if (this -> tileColour == BLACK)
    {
        resultChar = BLACK_TILE;
    }
    else if (this -> tileColour == FIRSTPLAYER)
    {
        resultChar = FIRSTPLAYER_TILE;
    }
    return resultChar;
}

std::string Tile::getColourStr()
{
    std::string resultStr = "NO_COLOUR";
    if (this -> tileColour == RED)
    {
        resultStr = "RED";
    }
    else if (this -> tileColour == DARKBLUE)
    {
        resultStr = "DARKBLUE";
    }
    else if (this -> tileColour == LIGHTBLUE)
    {
        resultStr = "LIGHTBLUE";
    }
    else if (this -> tileColour == YELLOW)
    {
        resultStr = "YELLOW";
    }
    else if (this -> tileColour == BLACK)
    {
        resultStr = "BLACK";
    }
    else if (this -> tileColour == FIRSTPLAYER)
    {
        resultStr = "FIRSTPLAYER";
    }
    return resultStr;
}