#include "Wall.h"

Wall::Wall()
{
    this -> tiles = nullptr;
}

Wall::Wall(Wall& other)
{
    this -> tiles = nullptr;
    if (other.tiles != nullptr)
    {
        this -> tiles = new Tile**[COLOUR_KIND];
        for (int i = 0; i < COLOUR_KIND; i++)
        {
            if (other.tiles[i] != nullptr)
            {
                this -> tiles[i] = new Tile*[COLOUR_KIND]{nullptr};
                for (int p = 0; p < COLOUR_KIND; p++)
                {
                    this -> tiles[i][p] = other.tiles[i][p];
                }
            }
            else
            {
                this -> tiles[i] = nullptr;
            }
        }

    }
}

Wall::~Wall()
{
    if (this -> tiles != nullptr)
    {
        for (int i = 0; i < COLOUR_KIND; i++)
        {
            if (this -> tiles[i] != nullptr)
            {
                delete[] this -> tiles[i];
                this -> tiles[i] = nullptr;
            }
            
        }
        delete[] this -> tiles;
        this -> tiles = nullptr;
    }
    
    
}

std::vector<Colour> Wall::getColours(int row)
{
    std::vector<Colour> result;
    if (this -> tiles != nullptr)
    {
        if (row >= 0 and row < COLOUR_KIND)
        {
            if (this -> tiles[row] != nullptr)
            {
                for (int i = 0; i < COLOUR_KIND; i++)
                {
                    if (this -> tiles[row][i] != nullptr)
                    {
                        result.push_back(this -> tiles[row][i] ->getColour());
                    }
                    
                }
            }
            
        }
        
    }
    return result;
    
    
}

int Wall::fillAndAddMark(Tile* payload, int row)
{
    int result = 0;
    if (payload != nullptr)
    {
        if 
        (
            colourIsValid(payload -> getColour()) && 
            row >= 0 && row < 5 && 
            std::find(this -> getColours(row).begin(),this -> getColours(row).end(),payload->getColour()) != this -> getColours(row).end()
        )
        {
            if (this -> tiles == nullptr)
            {
                this -> tiles = new Tile**[COLOUR_KIND];
                for (int i = 0; i < COLOUR_KIND; i++)
                {
                    this -> tiles[i] = new Tile*[COLOUR_KIND]{nullptr};
                }

            }
            int col = this -> findColourCol(payload -> getColour(),row);
            this -> tiles[row][col] = payload;

            // calculate the added points due to adding this tile
            bool verCon = false;
            bool horCon = false;
            int yMin = row-1;
            int yMax = row+1;
            int xMin = col-1;
            int xMax = col+1;
            while (this -> tiles[yMin][col] != nullptr && yMin >= 0)
            {
                verCon = true;
                result++;
                yMin--;
            }
            while (this -> tiles[yMax][col] != nullptr && yMax < COLOUR_KIND)
            {
                verCon = true;
                result++;
                yMax++;
            }
            while (this -> tiles[row][xMin] != nullptr && xMin >= 0)
            {
                horCon = true;
                result++;
                xMin--;
            }
            while (this -> tiles[row][xMax] != nullptr && xMax < COLOUR_KIND)
            {
                horCon = true;
                result++;
                xMax++;
            }
            result += (int)verCon + (int)horCon;
        }
        
    }
    return result;
}

int Wall::findColourCol(Colour colour, int row)
{
    int result = -1;
    if 
    (
        colour == DARKBLUE ||
        colour == YELLOW ||
        colour == RED ||
        colour == BLACK ||
        colour == LIGHTBLUE
    )
    {
        result = (row + colour) % 5;
    }
    return result;
}

std::string Wall::toString()
{
    std::string result = "Wall is empty";
    if (this -> tiles != nullptr)
    {
        result += "The wall has the following rows: \n";
        for (int i = 0; i < COLOUR_KIND; i++)
        {
            if (this -> tiles[i] == nullptr)
            {
                result += "The " + std::to_string(i) + "-th row of the wall is empty \n";
            }
            else
            {
                result += "The " + std::to_string(i) + "-th row of the wall has the following tiles in it: \n";
                for (int p= 0; p < COLOUR_KIND; p++)
                {
                    if (this -> tiles[i][p] == nullptr)
                    {
                        result += "EMPTY_SLOT\n";
                    }
                    else
                    {
                        result += this -> tiles[i][p] -> getColourStr() + "\n";
                    }
                }   
            } 
        }
    }
    return result;   
}