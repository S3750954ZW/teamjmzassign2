#include "Player.h"

Player::Player()
{
    this -> playerName.assign(DEFAULT_PLAYER_NAME);
    this -> score = 0;
    this -> hasToken = false;
    
    this -> patternline = nullptr;
    this -> floorline = nullptr;
    this -> wall = nullptr;
}
Player::Player(std::string name)
{
    this -> playerName = name;
    this -> score = 0;
    this -> hasToken = false;
    
    this -> patternline = nullptr;
    this -> floorline = nullptr;
    this -> wall = nullptr;    
}

Player::Player(Player& other)
{
    this -> playerName = other.playerName;
    this -> score = other.score;
    this -> hasToken = other.hasToken;
    
    this -> patternline = nullptr;
    this -> floorline = nullptr;
    this -> wall = nullptr;

    if (other.wall != nullptr)
    {
        this -> wall = new Wall(*other.wall);
    }
    if (other.floorline != nullptr)
    {
        this -> floorline = new Floorline(*other.floorline);
    }
    if (other.patternline != nullptr)
    {
        this -> patternline = new Patternline*[COLOUR_KIND]{nullptr};
        for (int i = 0; i < COLOUR_KIND; i++)
        {
            if (other.patternline[i] != nullptr)
            {
                this -> patternline[i] = new Patternline(*other.patternline[i]);
            }
        
        }
    }
}

Player::~Player()
{
    this -> playerName = "A worthy opponent";
    this -> score = 0;
    this -> hasToken = false;
    if (this -> wall != nullptr)
    {
        delete this -> wall;
        this -> wall = nullptr;
    }
    if (this -> floorline != nullptr)
    {
        delete this -> floorline;
        this -> floorline = nullptr;
    }
    if (this -> patternline != nullptr)
    {
        
        for (int i = 0; i < COLOUR_KIND; i++)
        {
            if (this -> patternline[i] != nullptr)
            {
                delete this -> patternline[i];
                this -> patternline[i] = nullptr;
            }
        }
        delete[] this -> patternline;
        this -> patternline = nullptr;
    }
}

void Player::setName(std::string name)
{
    if (!name.empty())
    {
        this -> playerName = name;
    }
    
}

std::string Player::getName()
{
    return this -> playerName;
}

bool Player::withToken()
{
    return this -> hasToken;
    
}

int Player::getScore()
{
    return this -> score;
    
}

Wall* Player::getWall()
{
    return this -> wall;
    
}

Patternline* Player::getPatternline(int row)
{
    Patternline* result = nullptr;
    if (row < COLOUR_KIND &&  row >= 0 && this -> patternline != nullptr)
    {
        result = this -> patternline[row];
    }
    return result;
    
}

Floorline* Player::getFloorline()
{
    return this -> floorline;
}

void Player::startRound()
{
    if (this -> wall == nullptr)
    {
        this -> wall = new Wall();
    }
    if (this -> floorline == nullptr)
    {
        this -> floorline = new Floorline();
    }
    if (this -> patternline == nullptr)
    {
        this -> patternline = new Patternline*[COLOUR_KIND];
        for (int i = 0; i < COLOUR_KIND; i++)
        {
            this -> patternline[i] = new Patternline();
            this -> patternline[i] -> setSizeCap(i+1);
        }
    }
    this -> hasToken = false;
    for (int i = 0; i < COLOUR_KIND; i++)
    {
        this -> patternline[i] ->unacceptColour(this -> wall ->getColours(i));
    }
    
}

std::vector<Tile*> Player::takeOffer(std::vector<Tile*> payload, MosaicStoreRow toRow)
{
    std::vector<Tile*> overflow;
    if (payload.size()>0)
    {
        if (this -> wall == nullptr)
        {
            this -> wall = new Wall();
        }
        if (this -> floorline == nullptr)
        {
            this -> floorline = new Floorline();
        }
        if (this -> patternline == nullptr)
        {
            this -> patternline = new Patternline*[COLOUR_KIND];
            for (int i = 0; i < COLOUR_KIND; i++)
            {
                this -> patternline[i] = new Patternline();
                this -> patternline[i] -> setSizeCap(i+1);
            }
        }
        for (int i = 0; i < (int)payload.size(); i++)
        {
            if (payload.at(i)->getColour() == FIRSTPLAYER)
            {
                this -> hasToken = true;
            }
        }
        if (toRow != BROKEN)
        {
            overflow = this -> patternline[toRow] -> fill(payload);
            if (overflow.size()>0)
            {
                overflow = this -> floorline -> fill(overflow);
            }
        }
        else
        {
            overflow = this -> floorline -> fill(payload);
        }
    }
    return overflow;
}

std::vector<Tile*> Player::finishRound()
{
    std::vector<Tile*> residue;
    // check patternlines one by one and move tiles to the wall
    if (this -> wall == nullptr)
    {
        this -> wall = new Wall();
    }
    if (this -> floorline == nullptr)
    {
        this -> floorline = new Floorline();
    }
    if (this -> patternline == nullptr)
    {
        this -> patternline = new Patternline*[COLOUR_KIND];
        for (int i = 0; i < COLOUR_KIND; i++)
        {
            this -> patternline[i] = new Patternline();
            this -> patternline[i] -> setSizeCap(i+1);
        }
    }
    std::vector<Tile*> payload;
    int addScore = 0;
    for (int i = 0; i < COLOUR_KIND; i++)
    {
        if (this -> patternline[i] -> size() >= this -> patternline[i] ->getSizeCap())
        {
            payload = this -> patternline[i] -> fetch(true);
            addScore = this -> wall -> fillAndAddMark(payload.at(0),i);
            if (addScore == 0)
            {
                residue.push_back(payload.at(0));
            }
            this -> score += addScore;
            addScore = 0;
            payload.clear();
            payload = this -> patternline[i] -> fetch();
            for (int p = 0; p < (int)payload.size(); p++)
            {
                residue.push_back(payload.at(p));
            }    
            payload.clear();      
        }        
        
    }
    this -> score += this -> floorline ->calculateMark();
    if (this -> score < 0)
    {
        this -> score = 0;
    }
    payload = this -> floorline -> fetch();
    for (int i = 0; i < (int)payload.size(); i++)
    {
        residue.push_back(payload.at(i));
    }
    payload.clear();

    return residue;
}


std::string Player::toString()
{
    std::string result = "Player is empty";
    if (this -> wall != nullptr)
    {
        result += "The player has the following wall: \n";
        result += this -> wall -> toString();
    }
    else
    {
        result =+ "this player's wall does not exist \n";
    }
    
    if (this -> floorline != nullptr)
    {
        result += "The player has the following floorline: \n";
        result += this -> floorline -> toString();
    }
    else
    {
        result =+ "this player's floorline does not exist \n";
    }
    

    if (this -> patternline != nullptr)
    {
        result += "The player has the following patterlines: \n";
        for (int i = 0; i < COLOUR_KIND; i++)
        {
            if (this -> patternline[i] != nullptr)
            {
                result += this -> patternline[i] -> toString();
            }
            else
            {
                result += "patternline #" + std::to_string(i) + " does not exist \n";
            }
        }
    }
    else
    {
        result =+ "this player's patternlines do not exist \n";
    }

    return result;
}
