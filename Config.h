#ifndef HEADER_CONFIG
#define HEADER_CONFIG
// config is used to set up MAGIC NUMBERS for the game
// such as total number of players, total number of tiles, total number of factory plates, mark rules
// lets make a safe assumption here that even for the individual milestone we might be asked
// to do up to customising number of players, total number of tiles and total number of factory plates
// and total number of tiles allowed in a factory plate

// #ifndef COSC_ASS_TWO_TYPES
// #define COSC_ASS_TWO_TYPES

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm> 
#include <sstream>
#include <limits>
#include <cstring>
#include <random>
#include <memory>

// standard output chars representation
#define RED_TILE         'R'
#define YELLOW_TILE      'Y'
#define DARKBLUE_TILE    'B'
#define LIGHTBLUE_TILE   'L'
#define BLACK_TILE       'U'
#define FIRSTPLAYER_TILE 'F'
#define NO_TILE          '.'
#define NO_COLOUR_CHAR '_'

// const variables
#define COLOUR_KIND 5
#define COLOUR_MAX 20
#define CENTRE_FACTORY_ID  0
#define TILE_MAX    COLOUR_MAX*COLOUR_KIND
#define PLAYER_MAX  2
#define FACTORY_MAX 6
#define OFFER_MAX   4
#define FLOOR_MAX   7
#define ROUND_MAX   5
#define SAVE_ENTRY_MIN 1+PLAYER_MAX
#define DEFAULT_FIRST_PLAYERID 0

#define DEFAULT_SAVE_MENU  "saves.azul"
#define DEFAULT_TILE_SEQUENCE "RYBLUYYBUBLLLURRLRLLRYBLUYYBUBLLLURYRLRLLRYBLUYYBUBLLLURRLRLLRYBLUYYBUBLLLURRLRLLRYBLUYBUBLLLURRLRLL"
#define DEFAULT_PLAYER_NAME "A worthy opponent"




typedef int PlayerID;
typedef int FactoryID;

enum Colour{
   DARKBLUE,
   YELLOW,
   RED,
   BLACK,
   LIGHTBLUE,
   FIRSTPLAYER,
   NO_COLOUR,
};

enum MosaicStoreRow{
   ONE,
   TWO,
   THREE,
   FOUR,
   FIVE,
   BROKEN
};
// #endif // COSC_ASS_TWO_TYPES

#endif // HEADER_CONFIG