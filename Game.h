#ifndef HEADER_GAME
#define HEADER_GAME

#include "Bag.h"
#include "Factory.h"
#include "Player.h"
#include "util.h"

// Game class
// contains all information for a specific game, main can use this to create and operate a game
// should provide service to main including all interactive features (i.e. input/output)
// has saving and loading methods for to get/update its key local variables and members
// can use all necessary methods provided by bag,lid,factory and player class to update its own variables
class Game
{
private:
    std::vector<Tile*> sourceTiles;
    Bag* bag;
    Player** players;
    Factory** factories;
    int roundNumber;
    std::string gameName;
    bool finished;
    bool testMode;
    PlayerID playerInMove;

    Bag* getBag();
    Factory* getFactory(FactoryID factoryID);
    Factory* getCentre();
    Player* getPlayer(PlayerID playerID);
    PlayerID whoMovesFirst();

public:

    Game();
    Game(std::string gameName, bool testMode = false);
    Game(Game& other);
    ~Game();

    /** 
     * load a sequence of cmd inputs to bring the game to a certain stage
     * would behave sightly different when the game is created under test mode or normal loading mode
     * majorly in terms of how the "end-of-loading" table is shown.
     * 
     * Load behaviours:
     * - Load will ignore all "save" cmds
     * - Load will return "fail" in string if the sequence is less than 3 rows
     * - Load will give game default settings and players defaultc names even if the first 3 rows are empty, as long as they exist
     * - Load will ignore invalid tile letters for the bag
     * - Load is compatible with both X86 and UNIX newline formats
     * - Load is compatible with sequences ending in both newlines and actual information
     * - Load is compatible with failed/invalid player inputs (will treat the next input as done by the same player until successful)
     * - Load in normal mode will not finish the game if it is less than 5 rounds and the cmd sequence has exhausted, will return the table for the next player in move ONLY and ready to enter a normal game process afterwards
     * - Load in test mode will finish the game no matter of the number of rounds, and will return the tables for both Players
     * - Load in normal mode will omit any excessive input sequence and end the game if the end-of-game is reached, and it will return the end-of-game report.
     * - Load in test mode will omit any excessive input sequence and end the game if the end-of-game is reached. And it will return both tables for both players at the end of the game, as well as the end-of-game report.
     */
    
    std::string load(std::string loadStr);
    
    // game functions
    void resetSourceTiles(std::string sourceTiles = "");
    void resetBag(std::vector<Tile*> payload);
    void resetFactories();
    void resetPlayers(std::string* playerNames = nullptr);
    void startGame();
    void startRound();
    void turns(PlayerID playerInMove, std::string playerCmd);
    bool gameCanFinish();
    bool roundCanFinish();
    void finishRound();
    void finishGame();
    
    // essential getters for main to access the game control and for the saving feature
    std::string getTableStr(bool testMode = false, bool endOfGame = false);
    bool isTestMode();
    bool isFinished();
    void setGameName(std::string newName = "unnamed game");
    std::string getGameName();
    int getGameRound();

};

#endif // HEADER_GAME