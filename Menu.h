#ifndef HEADER_MENU
#define HEADER_MENU


class Menu
{
private:
    int menuNumber;
public:
    Menu();
    ~Menu();
    void showWelcome();
    void showMenu();
    void showCredit();
    void menuSelect();
    void inputMenuNumber();
    int getMenuNumber();
};

#endif // HEADER_MENU
