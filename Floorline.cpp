#include "Floorline.h"

Floorline::Floorline()
{
    this -> tiles = nullptr;
    this -> minusArr[0] = -1;
    this -> minusArr[1] = -1;
    this -> minusArr[2] = -2;
    this -> minusArr[3] = -2;
    this -> minusArr[4] = -2;
    this -> minusArr[5] = -3;
    this -> minusArr[6] = -3;
}

Floorline::~Floorline()
{
    if (this -> tiles != nullptr)
    {
        delete this -> tiles;
        this -> tiles = nullptr;
    }
    
    this -> minusArr[0] = -1;
    this -> minusArr[1] = -1;
    this -> minusArr[2] = -2;
    this -> minusArr[3] = -2;
    this -> minusArr[4] = -2;
    this -> minusArr[5] = -3;
    this -> minusArr[6] = -3;
}

Floorline::Floorline(Floorline& other)
{
    this -> tiles = nullptr;
    if (other.tiles != nullptr)
    {
        this -> tiles = new std::vector<Tile*>;
        for (int i = 0; i < (int)other.tiles ->size(); i++)
        {
            this -> tiles -> push_back(other.tiles -> at(i));
        } 
    }
    for (int i = 0; i < FLOOR_MAX; i++)
    {
        this -> minusArr[i] = other.minusArr[i];
    }
}

Tile* Floorline::get(int index)
{
    Tile* result = nullptr;
    if (this -> tiles != nullptr)
    {
        if (index < (int)this -> tiles -> size() && index >= 0)
        {
            result = this -> tiles -> at(index);
        }
    }
    return result;
}

std::vector<Tile*> Floorline::fill(std::vector<Tile*> payload)
{
    std::vector<Tile*> overflow;
    if (this -> tiles == nullptr)
    {
        this -> tiles = new std::vector<Tile*>;
    }
    if (payload.size() > 0)
    {
        int currentSize = this -> tiles -> size();
        for (int i = currentSize; i < std::min(FLOOR_MAX,currentSize + (int)payload.size()); i++)
        {
            this -> tiles -> push_back(payload.at(i - currentSize));
        }
        for (int i = FLOOR_MAX; i < currentSize + (int)payload.size(); i++)
        {
            overflow.push_back(payload.at(i - FLOOR_MAX));
        }     
    }
    return overflow;
}

std::string Floorline::toString()
{    
    std::string result = "Floorline is empty";
    if (this -> tiles != nullptr)
    {
        if (this -> tiles -> size() > 0)
        {
            result = "The floorline has the following tiles in it \n";
            for (int i = 0; i < (int)this -> tiles -> size(); i++)
            {
                result += this -> tiles -> at(i) -> getColourStr() + "\n";
            }
        }
    }
    return result;
}

int Floorline::calculateMark()
{
    int result = 0;
    if (this -> tiles != nullptr)
    {
        if (this -> tiles -> size() > 0)
        {
            for (int i = 0; i < (int)this -> tiles -> size(); i++)
            {
                result += this -> minusArr[i];
            }
        }
    }
    return result;
}

int Floorline::size()
{
    int result = 0;
    if (this -> tiles != nullptr)
    {
        result = this -> tiles -> size();
    }
    return result;
}

void Floorline::clear()
{
    if (this -> tiles != nullptr)
    {
        if (this -> tiles -> size() > 0)
        {
            this -> tiles -> clear();
        }
        
    }
}

std::vector<Tile*> Floorline::fetch(bool excludeFirstPlayerToken/*=false*/)
{
    std::vector<Tile*> result;

    if (this -> tiles != nullptr)
    {
        if (this -> tiles -> size()>0)
        {
            int currentSize = this -> tiles -> size();
            if (excludeFirstPlayerToken)
            {
                for (int i = currentSize - 1; i >= 0; i--)
                {
                    if (this -> tiles -> at(i) -> getColour() != FIRSTPLAYER)
                    {
                        result.insert(result.begin(),this -> tiles -> at(i));
                        this -> tiles -> pop_back();
                    }
                }
            }
            else
            {
                for (int i = 0; i < currentSize; i++)
                {
                    result.push_back(this -> tiles -> at(i));
                }
                this -> tiles -> clear();
                
            }
            
        }
        
    }
    
    return result;
}