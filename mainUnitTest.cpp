#include "mainUnitTest.h"

int main(int argc, char* argv[])
{
    int len = 20;
    int plen = 3;
    char bagStr[len] = "RYBLUYYBBLLLURRLRLL";
    char payloadStr[plen] = "RR";
    std::vector<Tile*> tileIn;
    std::vector<Tile*> payload;
    for (int i = 0; i < len - 1; i++)
    {
        tileIn.push_back(new Tile(bagStr[i]));
    }
    for (int i = 0; i < plen -1; i++)
    {
        payload.push_back(new Tile(payloadStr[i]));
    }
    
    Game* testGame = new Game();

    std::cout << "test game reset all objects ..." << std::endl;
    testGame -> resetBag(tileIn);
    testGame -> resetFactories();
    testGame -> resetPlayers();
    std::cout << "game reset all objects done" << std::endl;

    std::cout << "test game -> bag all units ..." << std::endl;
    Bag* testBag = testGame -> getBag();
    testBag -> fill(tileIn);
    testBag -> toString();
    testBag -> size();
    testBag -> fetch(5);
    testBag -> clear();
    std::cout << "game -> bag all units done" << std::endl;
    testGame -> resetBag(tileIn);

    std::cout << "test game -> factory all units ..." << std::endl;
    Factory* testFactory = testGame -> getFactory(1);
    Factory* testCentre = testGame -> getCentre();
    testFactory -> checkCentre();
    testCentre -> checkCentre();
    testFactory -> fill(tileIn);
    testCentre -> fill(tileIn);
    testFactory -> toString();
    testCentre -> toString();
    testFactory -> isEmpty();
    testCentre -> isEmpty();
    testFactory -> fetch(RED);
    testCentre -> fetch();
    testFactory -> hasColour(RED);
    testCentre -> hasColour(RED);
    testFactory -> clear();
    testCentre -> clear();
    std::cout << "game -> factory all units done" << std::endl;
    testGame -> resetFactories();

    std::cout << "test game -> player all units ..." << std::endl;
    Player* testPlayer = testGame -> getPlayer(0);
    testPlayer ->startRound();
    testPlayer ->takeOffer(payload,TWO);
    testPlayer ->takeOffer(payload,BROKEN);
    testPlayer ->takeOffer(payload,THREE);
    testPlayer ->takeOffer(payload,ONE);
    testPlayer ->toString();
    testPlayer ->finishRound();
    testPlayer ->getFloorline();
    testPlayer ->getPatternline(0);
    testPlayer ->getScore();
    testPlayer ->getWall();
    testPlayer ->withToken();
    std::cout << "game -> player all units done" << std::endl;
    testGame -> resetPlayers();

    for (int i = 0; i < (int)tileIn.size(); i++)
    {
        delete tileIn.at(i);
        tileIn.at(i) = nullptr;
    }
    tileIn.clear();
    for (int i = 0; i < (int)payload.size(); i++)
    {
        delete payload.at(i);
        payload.at(i) = nullptr;
    }
    payload.clear();
    delete testGame;
    testGame = nullptr;

    return EXIT_SUCCESS;

}