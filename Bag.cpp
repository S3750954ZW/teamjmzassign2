#include "Bag.h"

Bag::Bag()
{
    this -> tiles = nullptr;
}

Bag::~Bag()
{
    if (this -> tiles != nullptr)
    {
        delete this -> tiles;
        this -> tiles = nullptr;
    }
    
}

Bag::Bag(Bag& other)
{
    this -> tiles = nullptr;
    if (other.tiles != nullptr)
    {
        this -> tiles = new LinkedList(*other.tiles);
    }
}

int Bag::size()
{
    int result = 0;
    if (this -> tiles != nullptr)
    {
        result = this -> tiles -> size();
    }
    return result;
}

std::vector<Tile*> Bag::fill(std::vector<Tile*> payload)
{
    std::vector<Tile*> residue;
    if (payload.size()>0)
    {
        if (this -> tiles == nullptr)
        {
            this -> tiles = new LinkedList();
        }
        for (int i = 0; i < (int)payload.size(); i++)
        {
            if (colourIsValid(payload.at(i)->getColour()))
            {
                this -> tiles -> addBack(payload.at(i));
            }
            else
            {
                residue.push_back(payload.at(i));
            }
        }
    }
    return residue;
}

void Bag::clear()
{
    if (this -> tiles != nullptr)
    {
        delete this -> tiles;
        this -> tiles = new LinkedList();
    }
}

std::vector<Tile*> Bag::fetch(int count /*=1*/)
{
    std::vector<Tile*> result;
    if (this -> tiles != nullptr)
    {
        for (int i = 0; i < std::min(count,this -> tiles -> size()); i++)
        {
            result.push_back(this -> tiles -> get(0));
            this -> tiles -> removeFront();
        }
    }
    return result;
}

std::string Bag::toString()
{
    std::string result = "Bag is empty";
    if (this -> tiles != nullptr)
    {
        if (this -> tiles -> size() > 0)
        {
            result = "The bag has the following tiles in it \n";
            for (int i = 0; i < this -> tiles -> size(); i++)
            {
                result += this -> tiles -> get(i) -> getColourStr() + "\n";
            }
        }
    }
    

    return result;

}