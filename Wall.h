#ifndef HEADER_WALL
#define HEADER_WALL

#include "Tile.h"
#include "util.h"
// Wall class
// be a square 2-d array of tile pointers - size can be initialized and fixed for the duration of a game
// size corresponds to COLOUR_KIND
// have comprehensive methods for player class operation features (count, get...)
// can implement methods add, calculate_mark


class Wall
{
private:
    Tile*** tiles;
    int findColourCol(Colour colour, int row);
public:
    Wall();
    Wall(Wall& other);
    ~Wall();
    /**
     * Get the occupied slots' colours in a vector for
     * a specific row of the Wall. Used to reject further
     * filling of the same colour of tiles into the same
     * row of patternlines (see patternline member vector : acceptable)
     */
    std::vector<Colour> getColours(int row);

    /**
     * fill the payload Tile ptr into the wall.
     * and calculate the score change due to this
     * operation. if payload is invalid to fill into
     * the wall, the score would be zero. This will
     * be used when fillAndAddMark() is called in player so
     * that the payload is further returned back to the bag
     * without needing to handle exceptions
     */
    int fillAndAddMark(Tile* payload, int row);

    std::string toString();
};

#endif // HEADER_WALL