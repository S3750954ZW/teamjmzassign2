#ifndef HEADER_FLOORLINE
#define HEADER_FLOORLINE

#include "Config.h"
#include "Tile.h"
// Floorline class


class Floorline
{
private:
    std::vector<Tile*>* tiles;
    int minusArr[FLOOR_MAX]; 
public:
    Floorline();
    ~Floorline();
    Floorline(Floorline& other);
    Tile* get(int index);
    
    /**
     * fill works in such a way that if payload is greater than
     * what could be contained in the floorline, then the overflow
     * will be given out 
     */
    std::vector<Tile*> fill(std::vector<Tile*> payload);
    
    /**
     * returns with the mark deduction incurred by current floorline
     * yet this does not purge the floorline
     * call fetch() to purge the floorline instead
     */
    int calculateMark();
    int size();

    /**
     * fetch by default all tiles from the floorline (if any)
     * but can be set to exclude the first player token so that
     * the fetched result can be returned back into bag directly
     * 
     * update: it looks like to exclude the first player token is
     * useless as the floorline would be operated to be fetched from
     * by the parent player object in one of its "wrapper" methods.
     * which cannot reach the bag and would complicate the implementation
     * logic too much if trying to return them separatly. Instead it
     * would be much more neat whenever an overflow/residue is given
     * with the first player token, the Game object check the stream
     * then separate the first player token.
     * 
     **/
    std::vector<Tile*> fetch(bool excludeFirstPlayerToken = false);
    void clear();

    std::string toString();
};

#endif // HEADER_FLOORLINE