#ifndef HEADER_NODE
#define HEADER_NODE

#include "Config.h"
#include "Tile.h"


class Node {
private:
    Tile* content;
	Node* next;
    Node* prev;

public:
	Node();
	Node(Tile* data);
    /*
     * Direction parameter is used as a flag
     * if p then this node can be attached to the tail
     * if n then this node can be attached to the head
     * if neither then this node is created as in Node(Tile* data)
     */
	Node(Tile* data, Node* attachTo, char direction);
    Node(Node& other);
    void setContent(Tile* data);
    void setAttach(Node* attachTo, char direction);
    Tile* getContent();
    Node* getAttach(char direction);
	~Node();
};


#endif // HEADER_LINK