#ifndef HEADER_PATTERNLINE
#define HEADER_PATTERNLINE

#include "Tile.h"

// Patternline class
// be a diagnal 2-d array of tile pointers - size can be initialized and fixed for the duration of a game
// number of rows corresponds to COLOUR_KIND
// have comprehensive methods for player class operation features (count, get...)
// can implement methods add, draw
// need to report errors if add does not comply with rules


class Patternline
{
private:
    std::vector<Tile*>* tiles; 
    int sizeCap;
    std::vector<Colour> acceptable;
public:
    Patternline();
    /**
     * Initialize a patternline with optional known sizeCap and its accept vector of colours
     */
    Patternline(int sizeCap, std::vector<Colour> acceptable);
    Patternline(Patternline& other);
    ~Patternline();
    /**
     * fill a patternline with tile(ptr)s, the fill operation will check for the following:
     * 1 - whether payload has anything 
     * 2 - whether tiles exist, if not create them on heap
     * 3 - whether the element in the payload is valid to be put in according to game colour rule and whether its a valid colour
     * 4 - whether the patternline is full already
     * 
     * All invalid colour would go into the return vector "overflow"
     * will allow only filled colour after valid fill
     * if no valid fill do nothing and return the payload as it is
     * 
     */
    std::vector<Tile*> fill(std::vector<Tile*> payload);
    /**
     * fetch tiles from the patternline. parameter fetchOne is default to false to fetch all tiles
     * when fetchOne is set to true only the first element of tile would be retreived into the return
     * fetch remove the original tile(ptr) from patternline
     * do nothing and return empty vector if nothing to fetch
     */
    std::vector<Tile*> fetch(bool fetchOne=false);
    void clear();
    /**
     * make the patternline to accept all colours
     * override its previous accepting colour list
     */
    void acceptAllColour();
    /**
     *  make the patternline accept no colour
     *  override its previous accepting colour list
     */
    void acceptNoColour();
    /**
     * unaccept specific colours
     * check for whether its a valid colour
     * only change the accepting colour list if the specific colour is in the accepting list
     * do nothing if colours do not exist in the list or is not a valid colour
     */
    void unacceptColour(std::vector<Colour> colours);
    /**
     * accepting only one colour
     * has to be a valid colour
     * override the previous accepting colour list
     * do nothing if its not a valid colour
     */
    void acceptOnlyColour(Colour colour);
    /**
     * get the list of acceptable colours
     */
    std::vector<Colour> getAcceptable();
    bool isAccepting(Colour);
    void setSizeCap(int sizeCap);
    int getSizeCap();
    int size();
    std::string toString();
};

#endif // HEADER_PATTERNLINE


