.default: all

all: Azul

test: mainUnitTest-AZUL.debug

clean:
	rm -f Azul *.o
	rm -f mainUnitTest-AZUL.debug *.o

Azul: Tile.o Bag.o Node.o LinkedList.o main.o Game.o Factory.o Floorline.o Patternline.o Wall.o util.o Player.o
	g++ -Wall -Werror -std=c++14 -g -O -o $@ $^

mainUnitTest-AZUL.debug: Tile.o Bag.o Node.o LinkedList.o Game.o Factory.o mainUnitTest.o Floorline.o Patternline.o Wall.o util.o Player.o
	g++ -Wall -Werror -std=c++14 -g -O -o $@ $^

%.o: %.cpp
	g++ -Wall -Werror -std=c++14 -g -O -c $^