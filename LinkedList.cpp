#include "LinkedList.h"

LinkedList::LinkedList() 
{
    this -> head = nullptr;
    this -> tail = nullptr;
    this -> len = 0;
}

LinkedList::LinkedList(LinkedList& other) 
{
    this -> head = nullptr;
    this -> tail = nullptr;
    this -> len = 0;
    if (other.len>0)
    {
        for (int i = 0; i < other.len; i++)
        {
            this -> addBack(other.get(i));
        }
        
    }
}

LinkedList::~LinkedList() {
   if (this -> len>0)
   {
       for (int i = 0; i < this -> len; i++)
       {
           this -> removeBack();
       } 
   }
}

int LinkedList::size() {
    return this -> len;
}

Tile* LinkedList::get(int index) {
    Tile* outPtr = nullptr;
    if (index < this -> len && index >= 0)
    {
       Node* target = nullptr;
       if (index <= this -> len / (int) 2)
       {
           target = this -> head;
           for (int i = 0; i < index; i++)
           {
               target = target -> getAttach('n');
           }
       }
       else
       {
           target = this -> tail;
           for (int i = 0; i < this -> len - index - 1; i++)
           {
               target = target -> getAttach('p');
           }
       }
       if (target != nullptr)
       {
           outPtr = target -> getContent();
       }
    }
    return outPtr;
}

void LinkedList::set(Tile* data, int index)
{
    if (index < this -> len && index >= 0)
    {
       Node* target = nullptr;
       if (index <= this -> len / (int) 2)
       {
           target = this -> head;
           for (int i = 0; i < index; i++)
           {
               target = target -> getAttach('n');
           }
       }
       else
       {
           target = this -> tail;
           for (int i = 0; i < index; i++)
           {
               target = target -> getAttach('p');
           }
       }
       if (target != nullptr)
       {
           target -> setContent(data);
       }
    }
    
}

void LinkedList::addFront(Tile* data) {
    if (this -> head == nullptr)
    {
        this -> head = new Node(data);
        if (this -> tail == nullptr)
        {
            this -> tail = this -> head;
        }  
    }
    else
    {
        Node* newHead = new Node(data,this -> head,'n');
        this -> head = newHead;
        newHead = nullptr;
        this -> head -> getAttach('n') -> setAttach(this -> head,'p');
    }
    this -> len ++;
}

void LinkedList::addBack(Tile* data) {
    if (this -> tail == nullptr)
    {
        this -> tail = new Node(data);
        if (this -> head == nullptr)
        {
            this -> head = this -> tail;
        }
    }
    else
    {
        Node* newTail = new Node(data,this -> tail,'p');
        this -> tail = newTail;
        newTail = nullptr;
        this -> tail -> getAttach('p') -> setAttach(this -> tail,'n');
    }
    this -> len ++;
}

void LinkedList::removeBack() {
    if (this -> len > 0)
    {
        if (this -> len == 1)
        {
            delete this -> tail;
            this -> tail = nullptr;
            delete this -> head;
            this -> head = nullptr;
        }
        else
        {
            Node* newTail = this -> tail -> getAttach('p');
            delete this -> tail;
            this -> tail = newTail;
            newTail = nullptr;
            this -> tail -> setAttach(nullptr, 'n');
        }
        
        this -> len --;   
    }
    
}


void LinkedList::removeFront() {
    if (this -> len > 0)
    {
        if (this -> len == 1)
        {
            delete this -> tail;
            this -> tail = nullptr;
            delete this -> head;
            this -> head = nullptr;
        }
        else
        {
            Node* newHead = this -> head -> getAttach('n');
            delete this -> head;
            this -> head = newHead;
            newHead = nullptr;
            this -> head -> setAttach(nullptr, 'p');
        }
        
        this -> len --;   
    }
}

void LinkedList::clear() {
    if (this -> len > 0)
    {
        Node* clearTarget = this -> head;
        for (int i = 0; i < this -> len; i++)
        {
            clearTarget -> setContent(nullptr);
            clearTarget = clearTarget -> getAttach('n');
        }
        
    }
    
}
