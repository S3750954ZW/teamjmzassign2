#include "util.h"

bool colourIsValid(Colour colour)
{
    bool result = false;
    if 
    (
        colour == RED ||
        colour == LIGHTBLUE ||
        colour == DARKBLUE ||
        colour == YELLOW ||
        colour == BLACK
    )
    {
        result = true;
    }
    return result;
}

bool colourCharIsValid(char colour)
{
    bool result = false;
    if 
    (
        colour == 'Y' ||
        colour == 'B' ||
        colour == 'L' ||
        colour == 'U' ||
        colour == 'R'
    )
    {
        result = true;
    }
    return result;
}

bool turnCmdFormat(std::string cmdIn){
	bool result = false;
	if(cmdIn.substr(0,4).compare("turn") == 0)
	{

		if (cmdIn.length() == 10)
		{
			if 
            ( 
                cmdIn[4] == ' ' 
                && cmdIn[6] == ' ' 
                && cmdIn[8] == ' '
                && 
                (
                    cmdIn[5] == '0'
                    || cmdIn[5] == '1'
                    || cmdIn[5] == '2'
                    || cmdIn[5] == '3'
                    || cmdIn[5] == '4'
                    || cmdIn[5] == '5'
                )
                && 
                (
                    cmdIn[7] == 'R'
                    || cmdIn[7] == 'Y'
                    || cmdIn[7] == 'B'
                    || cmdIn[7] == 'L'
                    || cmdIn[7] == 'U'
                )
                && 
                (
                    cmdIn[9] == '1'
                    || cmdIn[9] == '2'
                    || cmdIn[9] == '3'
                    || cmdIn[9] == '4'
                    || cmdIn[9] == '5'
                    || cmdIn[9] == 'B'
                )
            )
			{
				result = true;
			}
		}	
	}
    return result;
}

FactoryID parseFactoryIDChar(char data)
{
    FactoryID result = 0;
    if ((int)data - 48 >= 0 && (int)data - 48 <= 5)
    {
        result = (int)data - 48;
    }
    return result;    
}
Colour parseColourChar(char data)
{
    Colour result = NO_COLOUR;
    if (data == RED_TILE)
    {
        result = RED;
    }
    else if (data == YELLOW_TILE)
    {
        result = YELLOW;
    }
    else if (data == DARKBLUE_TILE)
    {
        result = DARKBLUE;
    }
    else if (data == LIGHTBLUE_TILE)
    {
        result = LIGHTBLUE;
    }
    else if (data == BLACK_TILE)
    {
        result = BLACK;
    }
    return result;
    

}
MosaicStoreRow parseRowChar(char data)
{
    int result = 5;
    if ((int)data - 49 >= 0 && (int)data - 49 <= 4)
    {
        result = (int)data - 49;
    }
    else if (data == 'B')
    {
        result = 5;
    }
    return (MosaicStoreRow)result; 

}