#include "Patternline.h"

Patternline::Patternline()
{
    this -> tiles = nullptr;
    this -> sizeCap = 0;
    this -> acceptable = {RED,YELLOW,DARKBLUE,LIGHTBLUE,BLACK};
}

Patternline::Patternline(int sizeCap, std::vector<Colour> acceptable)
{
    this -> tiles = nullptr;
    this -> sizeCap = sizeCap;
    for (int i = 0; i < (int)acceptable.size(); i++)
    {
        if 
        (
            acceptable.at(i) == RED ||
            acceptable.at(i) == LIGHTBLUE ||
            acceptable.at(i) == DARKBLUE ||
            acceptable.at(i) == YELLOW ||
            acceptable.at(i) == BLACK
        )
        {
            this -> acceptable.push_back(acceptable.at(i));
        }
    }
}

Patternline::Patternline(Patternline& other)
{
    this -> tiles = nullptr;
    if (other.tiles != nullptr)
    {
        this -> tiles = new std::vector<Tile*>;
        for (int i = 0; i < (int)other.tiles -> size(); i++)
        {
            this -> tiles -> push_back(other.tiles -> at(i));
        }
        
    }
    
    this -> sizeCap = other.sizeCap;
    this -> acceptable = other.acceptable;
}

Patternline::~Patternline()
{
    if (this -> tiles != nullptr)
    {
        delete this -> tiles;
        this -> tiles = nullptr;
    }
    this -> sizeCap = 0;
    if (this -> acceptable.size()>0)
    {
        this -> acceptable.clear();
        this -> acceptable = {RED,YELLOW,DARKBLUE,LIGHTBLUE,BLACK};
    }
}

std::vector<Tile*> Patternline::fill(std::vector<Tile*> payload)
{
    std::vector<Tile*> overflow;
    std::vector<Tile*> validIn;
    if (payload.size()>0)
    {
        for (int i = 0; i < (int)payload.size(); i++)
        {
            if 
            (
                std::find(this -> acceptable.begin(), this -> acceptable.end(), payload.at(i) -> getColour()) == this -> acceptable.end() &&
                (
                    payload.at(i) -> getColour() == RED ||
                    payload.at(i) -> getColour() == LIGHTBLUE ||
                    payload.at(i) -> getColour() == DARKBLUE ||
                    payload.at(i) -> getColour() == YELLOW ||
                    payload.at(i) -> getColour() == BLACK
                )
            )
            {
                validIn.push_back(payload.at(i));
            }
            else
            {
                overflow.push_back(payload.at(i));
            }
            
        }
        if (validIn.size()>0)
        {
            if (this -> tiles == nullptr)
            {
                this -> tiles = new std::vector<Tile*>;
            }
            for (int i = 0; i < (int)validIn.size(); i++)
            {
                if 
                (
                    std::find(this -> acceptable.begin(), this -> acceptable.end(), validIn.at(i) -> getColour()) == this -> acceptable.end() &&
                    (
                        validIn.at(i) -> getColour() == RED ||
                        validIn.at(i) -> getColour() == LIGHTBLUE ||
                        validIn.at(i) -> getColour() == DARKBLUE ||
                        validIn.at(i) -> getColour() == YELLOW ||
                        validIn.at(i) -> getColour() == BLACK
                    ) &&
                    (int)this -> tiles -> size() < this -> sizeCap
                )
                {
                    this -> tiles -> push_back(validIn.at(i));
                    this -> acceptOnlyColour(validIn.at(i) -> getColour());
                }
                else
                {
                    overflow.push_back(validIn.at(i));
                }
                
            }
            
        }
    }
    return overflow;
}

std::vector<Tile*> Patternline::fetch(bool fetchOne/*=false*/)
{
    std::vector<Tile*> result;
    if (this -> tiles != nullptr)
    {
        if (fetchOne)
        {
            if (this -> tiles -> size() > 0)
            {
                result.push_back(this -> tiles -> at(0));
                this -> tiles -> erase(this -> tiles -> begin());
            }
        }
        else
        {
            if (this -> tiles -> size() > 0)
            {
                for (int i = 0; i < (int)this -> tiles -> size(); i++)
                {
                    result.push_back(this -> tiles -> at(i));
                }
                this -> tiles -> clear();                
            }
            this -> acceptAllColour();
        }
    }

    return result;
}

void Patternline::clear()
{
    if (this -> tiles != nullptr)
    {
        if (this -> tiles -> size()>0)
        {
            this -> tiles -> clear();
        }
        
    }
    

}

void Patternline::acceptAllColour()
{
    this -> acceptable.clear();
    this -> acceptable = {RED,YELLOW,DARKBLUE,LIGHTBLUE,BLACK};

}

void Patternline::acceptNoColour()
{
    this -> acceptable.clear();
}

void Patternline::unacceptColour(std::vector<Colour> colours)
{
    if (colours.size()>0)
    {
        for (int i = 0; i < (int)colours.size(); i++)
        {
            if 
            (
                colours.at(i) == RED ||
                colours.at(i) == LIGHTBLUE ||
                colours.at(i) == DARKBLUE ||
                colours.at(i) == YELLOW ||
                colours.at(i) == BLACK
            )
            {
                if (this -> acceptable.size()>0)
                {
                    for (int p = 0; p < (int)this -> acceptable.size(); p++)
                    {
                        if (this -> acceptable.at(p) == colours.at(i))
                        {
                            this -> acceptable.erase(this -> acceptable.begin()+p);
                        }
                        
                    }
                    
                }
                

            }
        }
    }
}

void Patternline::acceptOnlyColour(Colour colour)
{
    if (colour == RED ||
        colour == LIGHTBLUE ||
        colour == DARKBLUE ||
        colour == YELLOW ||
        colour == BLACK)
    {
        this -> acceptable.clear();
        this -> acceptable.push_back(colour);
    }
}

std::vector<Colour> Patternline::getAcceptable()
{
    return this -> acceptable;
}

void Patternline::setSizeCap(int sizeCap)
{
    this -> sizeCap = sizeCap;
}

int Patternline::getSizeCap()
{
    return this -> sizeCap;
}

int Patternline::size()
{
    int result = 0;
    if (this -> tiles != nullptr)
    {
        result = this -> tiles -> size();
    }
    return result;

}

std::string Patternline::toString()
{
    std::string result = "Patternline is empty";
    if (this -> tiles != nullptr)
    {
        if (this -> tiles -> size() > 0)
        {
            result = "The patternline has the following tiles in it \n";
            for (int i = 0; i < (int)this -> tiles -> size(); i++)
            {
                result += this -> tiles -> at(i) -> getColourStr() + "\n";
            }
        }
    }
    

    return result;
}

bool Patternline::isAccepting(Colour data)
{
    bool result = true;
    if (!this -> acceptable.empty())
    {
        if (std::find(this -> acceptable.begin(),this -> acceptable.end(),data) != this -> acceptable.end())
        {
            result = false;
        }
        
    }
    return result;
    
}