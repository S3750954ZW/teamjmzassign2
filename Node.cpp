#include "Node.h"

Node::Node()
{
    this->content = nullptr;
    this->next = nullptr;
    this->prev = nullptr;
}

Node::Node(Tile* data)
{
    this->content = data;
    this->next = nullptr;
    this->prev = nullptr;
}

Node::Node(Node& other)
{
    this -> content = other.content;
    this -> next = other.next;
    this -> prev = other.prev;
}

Node::Node(Tile* data, Node* attachTo, char direction)
{
    this->content = data;
    this->next = nullptr;
    this->prev = nullptr;
    if (direction == 'p')
    {
        this->prev = attachTo;
    }
    else if (direction == 'n')
    {
        this->next = attachTo;
    }
}

Node::~Node()
{
    this->content = nullptr;
    this->next = nullptr;
    this->prev = nullptr;
}

void Node::setContent(Tile* data)
{
    this->content = data;
}

void Node::setAttach(Node* attachTo, char direction)
{
    if (direction == 'p')
    {
        this->prev = attachTo;
    }
    else if (direction == 'n')
    {
        this->next = attachTo;
    } 
}

Tile* Node::getContent()
{
    return this->content;
}

Node* Node::getAttach(char direction)
{
    Node* resultPtr = nullptr;
    if (direction == 'p')
    {
        resultPtr = this->prev;
    }
    else if (direction == 'n')
    {
        resultPtr = this->next;
    } 
    return resultPtr;
}