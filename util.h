#ifndef HEADER_UTIL
#define HEADER_UTIL

#include "Config.h"

bool colourIsValid(Colour);
bool colourCharIsValid(char);
bool turnCmdFormat(std::string);
FactoryID parseFactoryIDChar(char);
Colour parseColourChar(char);
MosaicStoreRow parseRowChar(char);
#endif