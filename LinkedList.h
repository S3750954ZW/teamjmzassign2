#ifndef LINKED_LIST_H
#define LINKED_LIST_H 

#include "Node.h"

class LinkedList {
public:
   LinkedList();
   /**
    * deep copy
    */
   LinkedList(LinkedList& other);
   /**
    * deep clean - heap operation - change linkage
    */
   ~LinkedList();

   /**
    * Return the current size of the Linked List.
    */
   int size();

   /**
    * Get the tile ptr at the given index.
    */
   Tile* get(int index);

   /**
    * Set the tile ptr at the given index - no heap opeartion for node - no change linkage
    */
   void set(Tile* data, int index);

   /**
    * Add the tile ptr to the back of the Linked List - heap operation for node - changes linkage
    */
   void addBack(Tile* data);

   /**
    * Add the tile ptr to the front of the Linked List - heap operation for node - changes linkage
    */
   void addFront(Tile* data);

   /**
    * Remove the tile ptr at the back of the Linked List - heap operation for node - changes linkage
    */
   void removeBack();

   /**
    * Remove the tile ptr at the front of the Linked List - heap operation for node - changes linkage
    */
   void removeFront();

   /**
    * Removes all tile ptr from the Linked List - no heap operation for node - no change linkage
    */
   void clear();

private:

   Node* head;
   Node* tail;
   int len;
};

#endif // LINKED_LIST_H
