#ifndef HEADER_PLAYER
#define HEADER_PLAYER
#include "Floorline.h"
#include "Patternline.h"
#include "Wall.h"

// Player class
// contains all information for a specific player, game class can use this to create array of players
// should provide service to game class including movements of tiles, update its private variables, error reports and printing the player card
// have saving and loading methods for game class to get/update its key local variables
// can use all necessary methods provided by patternline floorline wall class to update its own variables

class Player
{
private:
    std::string playerName;
    int score;
    bool hasToken;
    
    Patternline** patternline;
    Floorline* floorline;
    Wall* wall;
public:
    Player();
    Player(std::string name);
    Player(Player& other);
    ~Player();

    std::string getName();
    void setName(std::string Name);
    /**
     * Check the token flag, can be used at the beginning of
     * a round (by Game) to check who should go first
     * then the flag is reset to false by start the round
     */
    bool withToken();
    int getScore();
    Wall* getWall();
    Patternline* getPatternline(int row);
    Floorline* getFloorline();
    
    void startRound();
    /**
     * Please be vary that the overflow from takeoffer could contain the first player token
     * do NOT put it directly into bag. check for first player token first and put it aside 
     * before the next round refilling into the factory
     */
    std::vector<Tile*> takeOffer(std::vector<Tile*> payload, MosaicStoreRow toRow);
    /**
     * Please be vary that the residue from finishRound could contain the first player token
     * do NOT put it directly into bag. check for first player token first and put it aside 
     * before the next round refilling into the factory
     * Also if the payload contains the first player token, then the token flag for this
     * player would be set to true, to grant positive check for the player during 
     * the beginning check (by Game) next round
     */
    std::vector<Tile*> finishRound();

    std::string toString();
};

#endif // HEADER_PLAYER