#ifndef HEADER_FACTORY
#define HEADER_FACTORY

#include "Tile.h"

// Factory class

class Factory
{
private:
    std::vector<Tile*>* tiles;
    Tile* firstPlayerToken;
    bool isCentre;

public:
    Factory();
    Factory(std::vector<Tile*> payload);
    ~Factory();
    // make this factory become a centre factory (no size cap, can accept the first player token)
    void becomeCentre();
    // check whether this factory is a centre factory
    bool checkCentre();
    // clear everything from a factory including the first player token, yet not changing the centre flag
    void clear();
    /**
     * the payload gets filled into the factory
     * will return a vector of overflow if payload is too large
     * and if some invalid coloured tiles are included
     */
    std::vector<Tile*> fill(std::vector<Tile*> payload);

    /**
     * fetch all tiles from current factory including the first player token
     */
    std::vector<Tile*> fetch();
    /**
     * fetch specified coloured tiles from current factory
     * will return nothing if colour is invalid or does not exist
     * will automatically fetch the first player token if it is available in there
     */
    std::vector<Tile*> fetch(Colour tileColour);

    /**
     * check whether a specific colour exist in the factory
     * can be any colour including first player token and no_colour
     */
    bool hasColour(Colour tileColour);

    /**
     * check whether a factory is empty
     * factory with first player token only is 
     * also considered non-empty
     * to check for whether centre is with the token only
     * use hasColour to check
     */
    bool isEmpty();

    /**
     * presumably could be used to fill the centre factory with
     * the copied pointer to the first player token
     */
    Tile* getFirstPlayerToken();



    //Used to test and verify code
    std::string toString();
};




#endif // HEADER_FACTORY