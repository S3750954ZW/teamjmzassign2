#include <Game.h>
#include <Menu.h>

/** This holds the main logic of the game - acting as controller.
 * 
 */

int main() {
    Menu mainMenu = new Menu(); 

    mainMenu.showWelcome();
    mainMenu.showMenu();
    
    mainMenu.inputMenuNumber();
    mainMenu.menuSelect();

    // start NEW game
    if (mainMenu.getMenuNumber() == 1) {
        Game* newGame = new Game();
    }
    else {

    }
    return 0;
}