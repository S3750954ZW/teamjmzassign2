#include <iostream>
#include <Menu.h>

Menu::Menu() {
    this -> menuNumber = 0; 
}

void Menu::menuSelect() {
    if(menuNumber == 1) {
        std::cout << "Starting a New Game" << std::endl;
    }
    else if(menuNumber == 2) {
        std::cout << "Load Game" << std::endl;
    }
    else if (menuNumber == 3) {
        showCredit();
        showMenu();
    }
    else if (menuNumber == 4) {
        std::cout << "Quit" << std ::endl;
    }
}


void Menu::inputMenuNumber(){
    bool found = false;
    int a = 0;

    while (!found){
        std::cout << "> ";
        std::cin >> chosenNumber ;
        std::cout << std::endl;
        if(chosenNumber == 1
            ||chosenNumber == 2
            ||chosenNumber == 3
            ||chosenNumber == 4)
        {
            found = true;
        } 
        else {
            std::cin.clear();
            std::cin.ignore(100000, '\n');
            std::cout << "Invalid command" << std::endl;
            chosenNumber = 0;
        }
    }
    this -> menuNumber = chosenNumber;
}


void Menu::showWelcome() {
    std::cout << "################################" << std::endl ;
    std::cout << "WELCOME TO AZUL!" << std::endl;
    std::cout << "################################" << std::endl ;
}

void Menu::showMenu() {
    std::cout <<  std::endl;
    std::cout << "MAIN MENU" << std::endl;
    std::cout << "-----------------------" << std::endl;
    std::cout << "1. New Game" << std::endl;
    std::cout << "2. Load Game" << std::endl;
    std::cout << "3. Credits" << std::endl;
    std::cout << "4. Quit" << std::endl;
    std::cout << std::endl;
}

void Menu::showCredit() {
    std::cout << std::endl;
    std::cout << "-------------------" << std::endl;
    std::cout << "Name: Zhutian WANG" << std::endl;
    std::cout << "Student ID: s3750954" << std::endl;
    std::cout << "Email: s3750954@student.rmit.edu.au" << std::endl;
    std::cout << std::endl;
    std::cout << "Name: Marion Jae Padua" << std::endl;
    std::cout << "Student ID: s3508866" << std::endl;
    std::cout << "Email: s3508866@student.rmit.edu.au" << std::endl;
    std::cout << std::endl;
    std::cout << "Name: zijun jia" << std::endl;
    std::cout << "Student ID: s3695944 " << std::endl;
    std::cout << "Email: s3695944@student.rmit.edu.au" << std::endl;
    std::cout << "-------------------" << std::endl << std::endl;
}


int Menu::getMenuNumber() {
    return menuNumber; 
}